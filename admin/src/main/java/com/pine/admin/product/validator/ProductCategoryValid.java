package com.pine.admin.product.validator;

import lombok.Data;

import java.io.Serializable;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author liuzs
 * @date 2020/09/30
 */
@Data
public class ProductCategoryValid implements Serializable {
    @NotEmpty(message = "类别名称不能为空")
    private String name;
    @NotNull(message = "父ID不能为空")
    @Digits(integer = 12, fraction = 2, message = "父ID不是数字")
    private Long parent_id;
    @Digits(integer = 12, fraction = 2, message = "层级不是数字")
    private Integer level;
    @Digits(integer = 12, fraction = 2, message = "产品数量不是数字")
    private Integer product_count;
    @Digits(integer = 12, fraction = 2, message = "导航显示不是数字")
    private Integer nav_status;
    @Digits(integer = 12, fraction = 2, message = "显示状态不是数字")
    private Integer show_status;
    @Digits(integer = 12, fraction = 2, message = "排序不是数字")
    private Integer sort;
}