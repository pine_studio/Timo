package com.pine.admin.product.controller;

import com.pine.common.enums.StatusEnum;
import com.pine.common.utils.EntityBeanUtil;
import com.pine.common.utils.ResultVoUtil;
import com.pine.common.utils.StatusUtil;
import com.pine.common.vo.ResultVo;
import com.pine.admin.product.validator.ProductCategoryValid;
import com.pine.modules.product.domain.ProductCategory;
import com.pine.modules.product.service.ProductCategoryService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author liuzs
 * @date 2020/09/30
 */
@Controller
@RequestMapping("/product/category")
public class ProductCategoryController {

    @Autowired
    private ProductCategoryService productCategoryService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("product:category:index")
    public String index(Model model, ProductCategory productCategory) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", match -> match.contains());

        // 获取数据列表
        Example<ProductCategory> example = Example.of(productCategory, matcher);
        Page<ProductCategory> list = productCategoryService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/product/category/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("product:category:add")
    public String toAdd() {
        return "/product/category/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("product:category:edit")
    public String toEdit(@PathVariable("id") ProductCategory productCategory, Model model) {
        model.addAttribute("category", productCategory);
        return "/product/category/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"product:category:add", "product:category:edit"})
    @ResponseBody
    public ResultVo save(@Validated ProductCategoryValid valid, ProductCategory productCategory) {
        // 复制保留无需修改的数据
        if (productCategory.getId() != null) {
            ProductCategory beProductCategory = productCategoryService.getById(productCategory.getId());
            EntityBeanUtil.copyProperties(beProductCategory, productCategory);
        }

        // 保存数据
        productCategoryService.save(productCategory);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("product:category:detail")
    public String toDetail(@PathVariable("id") ProductCategory productCategory, Model model) {
        model.addAttribute("productCategory",productCategory);
        return "/product/category/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("product:category:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (productCategoryService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}