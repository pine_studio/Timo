package com.pine.modules.product.repository;

import com.pine.modules.system.repository.BaseRepository;
import com.pine.modules.product.domain.ProductCategory;

/**
 * @author liuzs
 * @date 2020/09/30
 */
public interface ProductCategoryRepository extends BaseRepository<ProductCategory, Long> {
}