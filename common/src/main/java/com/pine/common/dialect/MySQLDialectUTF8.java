package com.pine.common.dialect;

import org.hibernate.dialect.MySQL57Dialect;

/**
 * 重写数据库方言，设置默认字符集为utf8
 * @author pine
 * @date 2018/10/15
 */
public class MySQLDialectUTF8 extends MySQL57Dialect {

    @Override
    public String getTableTypeString() {
        return " ENGINE=InnoDB DEFAULT CHARSET=utf8";
    }
}
